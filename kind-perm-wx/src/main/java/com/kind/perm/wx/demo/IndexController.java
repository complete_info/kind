package com.kind.perm.wx.demo;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kind.common.uitls.WxUtils;
import com.kind.perm.core.account.domain.AccountDO;
import com.kind.perm.wx.common.controller.BaseController;

/**
 *
 * 主页控制器. <br/>
 *
 * @date:2017年03月07日 上午15:03:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller
@RequestMapping("/index/")
public class IndexController extends BaseController {

	
	/**
	 * 引导主页.
	 *
	 * @param model
	 */
	@RequestMapping(value = "guide", method = RequestMethod.GET)
	public String toguide(Model model, HttpSession session) {

		//通过微信授权的获取到的对应的用户信息
		AccountDO account = (AccountDO)session.getAttribute(WxUtils.WX_session_account);
		
		return "angel2/yindaoye";
	}


}
