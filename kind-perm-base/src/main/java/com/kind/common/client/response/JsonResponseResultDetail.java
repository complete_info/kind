package com.kind.common.client.response;

/**
 * Json响应结果对象【对象类型】
 * 
 * @author 李明
 *
 */
public class JsonResponseResultDetail<T> {

    /** 响应业务状态码[0-成功 非0-失败] */
    private Integer ret;
    /** 响应消息 */
    private String msg;
    /** 响应中的数据 */
    private T data;
    
    public JsonResponseResultDetail(Integer ret, String msg, T data) {
        this.ret = ret;
        this.msg = msg;
        this.data = data;
    }
    
    public JsonResponseResultDetail(Integer ret, String msg) {
        this.ret = ret;
        this.msg = msg;
        this.data = null;
    }

    public JsonResponseResultDetail(T data) {
        this.ret = 0;
        this.msg = "OK";
        this.data = data;
    }

    public JsonResponseResultDetail() {

    }

    public Integer getRet() {
        return ret;
    }

    public void setRet(Integer ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
