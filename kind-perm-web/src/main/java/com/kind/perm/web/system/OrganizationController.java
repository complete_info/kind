package com.kind.perm.web.system;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kind.common.client.response.JsonResponseResult;
import com.kind.common.contants.Constants;
import com.kind.common.dto.DataGridResult;
import com.kind.common.enums.ResponseState;
import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageView;
import com.kind.common.uitls.DateUtils;
import com.kind.common.uitls.NumberUtils;
import com.kind.perm.core.system.domain.AreaDO;
import com.kind.perm.core.system.domain.FileDO;
import com.kind.perm.core.system.domain.OrganizationDO;
import com.kind.perm.core.system.domain.PermissionDO;
import com.kind.perm.core.system.domain.TableCustomTempletDO;
import com.kind.perm.core.system.service.AreaService;
import com.kind.perm.core.system.service.CoFileService;
import com.kind.perm.core.system.service.OrganizationService;
import com.kind.perm.core.system.service.TableCustomService;
import com.kind.perm.core.system.service.TableCustomTempletService;
import com.kind.perm.web.common.ExcelUtils;
import com.kind.perm.web.common.controller.BaseController;

/**
 * 
 * Function:机构信息管理控制器. <br/>
 * 
 * @date:2016年5月12日 上午11:18:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller
@RequestMapping("system/organization")
public class OrganizationController extends BaseController {
	
	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private CoFileService coFileService;
	
	@Autowired
	private TableCustomService tableCustomService;
	
	@Autowired
	private TableCustomTempletService tableCustomTempletService;
	
	@Autowired
	private AreaService areaService;

    /**列表页面*/
    private final String LIST_PAGE = "system/organization/organization_list";
    /**表单页面*/
    private final String FORM_PAGE = "system/organization/organization_form";

	/**
	 * 进入到默认列表页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String toListPage() {
		return LIST_PAGE;
	}

	
	/**
	 * 全部机构数据.
	 * @return
	 */
	@RequiresPermissions("system:organization:view")
	@RequestMapping(value = "json", method = RequestMethod.GET)
	@ResponseBody
	public List<OrganizationDO> getAllMenus() {
		List<OrganizationDO> organizationList = organizationService.queryList(new OrganizationDO());
		return organizationList;
	}
	
	/**
	 * 获取分页查询列表数据.
	 */
	@RequestMapping(value = "selectPageList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridResult selectPageList(OrganizationDO query, HttpServletRequest request) {
//		String sort = request.getParameter("sort");
//		String order = request.getParameter("order");
//		query.setOrder(order);
//		query.setSort(sort);
		PageView<OrganizationDO> page = organizationService.selectPageList(query);
		return super.buildDataGrid(page);
	}

	/**
	 * 加载添加页面.
	 * 
	 * @param model
	 */
	@RequiresPermissions("system:organization:save")
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String toAdd(Model model) {
		model.addAttribute("entity", new OrganizationDO());
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_ADD);
		
		//获取地区数据
		List<AreaDO> areaList =  areaService.queryByPcode("100000");
		model.addAttribute("areaList", areaList);
		
		return FORM_PAGE;
	}

	/**
	 * 保存数据.
	 * 
	 * @param entity
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult add(@Valid OrganizationDO entity, Model model, HttpServletRequest request) {
        try {
            organizationService.save(entity);
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 加载修改页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("system:organization:change")
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {	
		updateView(id, model);
		return FORM_PAGE;
	}

	/**
	 * 修改数据.
	 * 
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult update(@Valid @ModelAttribute @RequestBody OrganizationDO entity, Model model, HttpServletRequest request) {
        try {
            organizationService.save(entity);           
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 删除数据.
	 * 
	 * @param id
	 * @return
	 */
	@RequiresPermissions("system:organization:remove")
	@RequestMapping(value = "remove/{id}")
	@ResponseBody
	public String remove(@PathVariable("id") Long id) {
	    try {
            if (!NumberUtils.isEmptyLong(id)) {
                organizationService.remove(id);
            }
            return "success";

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return "error";
        }
	}
	
	/**
	 * 加载查看页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("system:organization:view")
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, Model model) {
		model.addAttribute("view", "true");
		updateView(id, model);

		return FORM_PAGE;
	}
	
	/**
	 * 导出信息
	 * @param ids
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("system:organization:export")
	@RequestMapping("export")
	@ResponseBody
	public String export(OrganizationDO query,HttpServletRequest request,HttpServletResponse response){
		TableCustomTempletDO entity = new TableCustomTempletDO();
		entity.setJavaBean("OrganizationDO");
		List<TableCustomTempletDO> tableCustomTempletlist = tableCustomTempletService.queryList(entity);
		TableCustomTempletDO tableCustomTempletDO = new TableCustomTempletDO();
		if(tableCustomTempletlist.size()>0)
		{
			tableCustomTempletDO = tableCustomTempletlist.get(0);
		}
		else
		{
			return "未设置信息";
		}
		return ExcelUtils.export(organizationService.queryList(query), tableCustomService.findTableCustomExport(tableCustomTempletDO.getType()), tableCustomTempletDO.getName()+DateUtils.getDateRandom()+".xls", request, response);
	
	}
	
	private void updateView(Long id, Model model) {
		model.addAttribute("entity", organizationService.getById(id));
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_UPDATE);
		//获取附件列表   start
		//提醒需要在com.kind.common.contants中加入对应的实体的常量
		// public final static String COFILE_ORGANIZATION = "OrganizationDO";
		String objectB =Constants.COFILE_ORGANIZATION;
		String objectA =Constants.COFILE_FILE;
		List<FileDO> filelist =  coFileService.getBatch(objectA, objectB, id);
		StringBuffer fileIds = new StringBuffer();
		for (FileDO fileDO : filelist) {
			fileIds.append(fileDO.getId()+",");
		}
		model.addAttribute("filelist", filelist);
		model.addAttribute("fileIds", fileIds);
		//获取附件列表   end
		
		//获取地区数据
		List<AreaDO> areaList =  areaService.queryByPcode("100000");
		model.addAttribute("areaList", areaList);
	}
	
	/**
	 * 获取areaId 代表的  省 市  区
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("system:organization:change")
	@RequestMapping(value = "getArea/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getArea(@PathVariable("id") Long id, Model model) {	
		String str = "";
		AreaDO area = areaService.getById(id);
		if(area != null && "3".equals(area.getLevelType())){
			AreaDO areaCity = areaService.queryByCode(area.getPcode());
			AreaDO areaProvince = areaService.queryByCode(areaCity.getPcode());
			str = areaProvince.getId()+","+areaCity.getId()+","+area.getId();
		}else if(area != null && "2".equals(area.getLevelType())){
			AreaDO areaProvince = areaService.queryByCode(area.getPcode());
			str = areaProvince.getId()+","+area.getId()+",0";
		}else if(area != null && "1".equals(area.getLevelType())){
			
			str = area.getId()+",0,0";
		}
		
		return str;
	}

}
