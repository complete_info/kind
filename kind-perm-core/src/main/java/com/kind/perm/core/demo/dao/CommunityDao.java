package com.kind.perm.core.demo.dao;

import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.perm.core.demo.domain.CommunityDO;

/**
 * Function:小区数据访问接口. <br/>
 * @date:2016年12月12日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public interface CommunityDao {

    final String NAMESPACE = "com.kind.perm.core.mapper.demo.CommunityDOMapper.";

	/**
	 * 分页查询的数据<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	List<CommunityDO> page(PageQuery pageQuery);

	/**
	 * 分页查询的数据记录数<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	int count(PageQuery pageQuery);

	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(CommunityDO entity);

	/**
	 * 根据id获取数据对象<br/>
	 *
	 * @param id
	 * @return
	 */
	CommunityDO getById(Long id);

    /**
     * 删除数据 <br/>
     *
     * @param id
     */
    void remove(Long id);
    
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<CommunityDO> queryList(CommunityDO entity);
}
