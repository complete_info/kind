package com.kind.perm.core.system.dao;

import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.system.domain.FileDO;

/**
 * Function:小区数据访问接口. <br/>
 * @date:2016年12月12日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public interface FileDao {

    final String NAMESPACE = "com.kind.perm.core.mapper.system.FileDOMapper.";

	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(FileDO entity);
	
	/**
	 * 根据id获取数据对象<br/>
	 *
	 * @param id
	 * @return
	 */
	FileDO getById(Long id);
	
    /**
     * 删除数据 <br/>
     *
     * @param id
     */
    void remove(Long id);

}
