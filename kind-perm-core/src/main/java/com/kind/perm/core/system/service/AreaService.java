package com.kind.perm.core.system.service;

import java.util.List;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.system.domain.AreaDO;

/**
 * 区域信息业务处理接口<br/>
 *
 * @Date: 2017-03-01 09:53:29
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface AreaService {

    /**
     * 分页查询
     * @param pageQuery
     * @return
     */
	PageView<AreaDO> selectPageList(PageQuery pageQuery);

    /**
     * 保存数据
     * @param entity
     */
	int save(AreaDO entity);

    /**
     * 获取数据对象
     * @param id
     * @return
     */
    AreaDO getById(Long id);

    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<AreaDO> queryList(AreaDO entity);
	
	/**
	 * 根据父code 查询
	 * @param pcode
	 * @return
	 */
	List<AreaDO> queryByPcode(String pcode);

	/**
	 * 根据code 查询
	 * @param code
	 * @return
	 */
	AreaDO queryByCode(String code);
}
